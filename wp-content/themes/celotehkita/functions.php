<?php
/**
 * @package WordPress
 * @subpackage CelotehKita
 * @since CelotehKita 1.0
 */

if ( ! function_exists( 'celotehkita_setup' ) ) :
	function celotehkita_setup() 
	{
	    show_admin_bar(false);
		// Add RSS feed links to <head> for posts and comments.
		add_theme_support( 'automatic-feed-links' );

		// Enable support for Post Thumbnails, and declare two sizes.
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 672, 372, true );
		add_image_size( 'celotehkita-full-width', 1038, 576, true );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list',
		) );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		// add_theme_support( 'post-formats', array(
			// 'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
		// ) );

		// This theme allows users to set a custom background.
		add_theme_support( 'custom-background', apply_filters( 'twentyfourteen_custom_background_args', array(
			'default-color' => 'f5f5f5',
		) ) );

		add_action( 'init', 'taxonomi_layout' );
		add_action( 'init', 'custom_css_editor' );

		// This theme uses its own gallery styles.
		// add_filter( 'use_default_gallery_style', '__return_false' );

		add_action( 'show_user_profile', 'custom_background_for_author_page' );
		add_action( 'edit_user_profile', 'custom_background_for_author_page' );

		add_action( 'personal_options_update', 'save_custom_background_for_author_page' );
		add_action( 'edit_user_profile_update', 'save_custom_background_for_author_page' );
	}
endif; // twentyfourteen_setup

add_action( 'after_setup_theme', 'celotehkita_setup' );

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since CelotehKita 1.0
 *
 * @return void
 */
function celotehkita_scripts() 
{
	// Load our main stylesheet.
	wp_enqueue_style( 'celotehkita-style', get_stylesheet_uri() );
	wp_enqueue_style( 'celotehkita-style-media', get_template_directory_uri().'/style_media.css', array('celotehkita-style'), null);
	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'celotehkita-ie', get_template_directory_uri() . '/css/ie.css', array( 'celotehkita-style'), '20131205' );
	wp_style_add_data( 'celotehkita-ie', 'conditional', 'lt IE 9' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
    wp_deregister_script('jquery');
    wp_register_script('jquery', get_template_directory_uri() . "/js/jquery-1.8.2.min.js", false, null);
    wp_enqueue_script('jquery');
	wp_enqueue_script( 'celotehkita-plugins-script', get_template_directory_uri() . '/js/plugins.js', array( 'jquery' ), '20131209', true );
	wp_enqueue_script( 'celotehkita-engine-script', get_template_directory_uri() . '/js/engine.js', array( 'jquery' ), '20131209', true );
	wp_enqueue_script( 'celotehkita-engine-layout-script', get_template_directory_uri() . '/js/layout-engine.js', array( 'jquery' ), '20131209', true );
}
add_action( 'wp_enqueue_scripts', 'celotehkita_scripts' );

if( ! function_exists('celotehkita_admin_stuff')) : 

endif;

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function celotehkita_wp_title( $title, $sep ) 
{
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'celotehkita' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'celotehkita_wp_title', 10, 2 );

function custom_background_for_author_page( $user ) 
{
?>
	<h3><?php _e('Image URL for author page', 'background_url'); ?></h3>
	
	<table class="form-table">
		<tr>
			<th>
				<label for="image_url"><?php _e('Image URL', 'background_url'); ?>
			</label></th>
			<td>
				<input type="text" name="image_url" id="image_url" value="<?php echo esc_attr( get_the_author_meta( 'image_url', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your image url.', 'background_url'); ?></span>
			</td>
		</tr>
	</table>
<?php }

function save_custom_background_for_author_page( $user_id ) 
{
	if ( !current_user_can( 'edit_user', $user_id ) )
		return FALSE;
	
	update_usermeta( $user_id, 'image_url', $_POST['image_url'] );
}

// indramdhani : style to make frontend and backend same
function custom_css_editor()
{
    add_editor_style(get_template_directory_uri() . "/custom_admin_editor.css");
}   

// indramdhani : buat taxonomi untuk post
/**
 * Create a taxonomy
 *
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 * @return null|WP_Error WP_Error if errors, otherwise null.
 */
function taxonomi_layout() 
{
	$labels = array(
		'name'					=> _x( 'Layouts', 'Taxonomy plural name', 'text-domain' ),
		'singular_name'			=> _x( 'Layout', 'Taxonomy singular name', 'text-domain' ),
		'search_items'			=> __( 'Search Layouts', 'text-domain' ),
		'popular_items'			=> __( 'Popular Layouts', 'text-domain' ),
		'all_items'				=> __( 'All Layouts', 'text-domain' ),
		'parent_item'			=> __( 'Parent Layout', 'text-domain' ),
		'parent_item_colon'		=> __( 'Parent Layout', 'text-domain' ),
		'edit_item'				=> __( 'Edit Layout', 'text-domain' ),
		'update_item'			=> __( 'Update Layout', 'text-domain' ),
		'add_new_item'			=> __( 'Add New Layout', 'text-domain' ),
		'new_item_name'			=> __( 'New Layout', 'text-domain' ),
		'add_or_remove_items'	=> __( 'Add or remove Layouts', 'text-domain' ),
		'choose_from_most_used'	=> __( 'Choose from most used text-domain', 'text-domain' ),
		'menu_name'				=> __( 'Layout', 'text-domain' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => false,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => true,
		'query_var'         => true,
		'capabilities'      => array(
                'manage_terms' => 'manage_categories',
                'edit_terms' => 'manage_categories',
                'delete_terms' => 'manage_categories',
                'assign_terms' => 'edit_posts',
            ),
	);

	register_taxonomy( 'Layout', array( 'post' ), $args );
}


// indramdhani : change category from check box to radio 
if(strstr($_SERVER['REQUEST_URI'], 'wp-admin/post-new.php') || strstr($_SERVER['REQUEST_URI'], 'wp-admin/post.php')) 
{
    ob_start('one_category_only');
}

function one_category_only($content) 
{
   	$content = str_replace('type="checkbox" ', 'type="radio" ', $content);
	return $content;
}

// indramdhani : fungsi buat ambil data2 post berdasar id
function celotehkita_get_post_data()
{
	// query_posts("p=" . $id);
	// if ( have_posts() ) : the_post();
		$dataToSend['title'] = get_the_title();
		$dataToSend['title'] = get_the_title();
		$dataToSend['author'] = get_the_author();
		$dataToSend['author_id'] = get_author_posts_url( get_the_author_meta( 'ID' ) );
		$dataToSend['id'] = get_the_id();
		$dataToSend['date'] = get_the_date();
		$dataToSend['excerpt'] = get_the_excerpt();
		// $dataToSend['content'] = get_the_content();
		$dataToSend['permalink'] = get_permalink();
		// indramdhani : get media
		$image = get_attached_media( 'image', get_the_id());
		$video = get_attached_media( 'video', get_the_id());
		$audio = get_attached_media( 'audio', get_the_id());
		$url = get_attached_media( 'url', get_the_id());
		// echo json_encode($image);
		// echo json_encode($video);
		// echo json_encode($audio);
		// echo json_encode($url);
		// indramdhani : change for layout data
		$layout_temp = '';
		$layout_slug = '';
		if(get_the_terms(get_the_id(), 'Layout'))
		{
			foreach (get_the_terms(get_the_id(), 'Layout') as $key => $value) 
			{
				$layout_temp = $value->name;
				$layout_slug = $value->slug;
			}
		}
		$dataToSend['layout_name'] = $layout_temp;
		$dataToSend['layout_slug'] = $layout_slug;
		$dataToSend['layout_to_get'] = $layout_slug;
		
		// indramdhani : change for categories data
		$category_temp = '';
		$category_slug = '';
		if(get_the_terms(get_the_id(), 'category'))
		{
			foreach (get_the_terms(get_the_id(), 'category') as $key => $value) 
			{
				$category_temp[] = $value->name;
				$category_slug[] = $value->slug;
			}
		}
		$dataToSend['categories_name'] = $category_temp;
		$dataToSend['categories_slug'] = $category_slug;
		// indramdhani : change for tag data

		$tags_temp = '';
		$tags_slug = '';
		if(get_the_tags(get_the_id()))
		{
			foreach (get_the_tags(get_the_id()) as $key => $value) 
			{
				$tags_temp[] = $value->name;
				$tags_slug[] = $value->slug;
			}
		}
		$dataToSend['tags_name'] = $tags_temp;
		$dataToSend['tags_slug'] = $tags_slug;
		wp_reset_query();
		return $dataToSend;
	// endif;
}

function celotehkita_permalink_title()
{
	$link = "<a href=" . get_permalink() . ">" . get_the_title() . "</a>";
	echo $link;
}

function celotehkita_get_title()
{
	$txt_title = get_the_title();
	$txt_title = explode(',', $txt_title);
	if(sizeof($txt_title) == 1)
	{
		echo '<div class="top-header">' . trim($txt_title[0]) . '</div>';
	}
	else
	{
		echo '<div class="top-header">' . trim($txt_title[0]) . '</div>';
	 	echo '<div class="bottom-header">' . trim($txt_title[1]) . '</div>';
	}
}

function celotehkita_get_layout()
{
	$layout_slug = '';
	if(get_the_terms(get_the_id(), 'Layout'))
	{
		foreach (get_the_terms(get_the_id(), 'Layout') as $key => $value) 
		{
			$layout_slug = $value->slug;
		}
	}
	return $layout_slug;
}

function celotehkita_get_post_meta()
{
	$posted_date = "Posted in " . get_the_date();
	$posted_by = "By <a href=" . get_author_posts_url( get_the_author_meta( 'ID' ) ) . ">" . get_the_author() . "</a>"; 
	echo "<br/>".$posted_by .'<br/>' . $posted_date;
}

function celotehkita_get_author_name()
{
	echo "<a href=" . get_author_posts_url(get_the_author_meta( 'ID' ) ) . ">" . get_the_author() . "</a>";	
}

function celotehkita_get_author_bio()
{
	the_author_meta('description');
}

function celotehkita_get_twitter($use_icon = false, $add_class='')
{
	$twitter_username = get_the_author_meta('twitter');
	if($use_icon)
	{
		echo "<a href='http://www.twitter.com/" . $twitter_username  . "'><img class='" . $add_class . "' src='" . get_template_directory_uri() . "/images/twitter.png'></a>";
	}
	else
	{
		echo "<a href='http://www.twitter.com/" . $twitter_username  . "'>@". $twitter_username."</a>";
	}
}

function celotehkita_get_google($use_icon = false, $add_class='')
{
	$google_link = get_the_author_meta('googleplus');
	if($use_icon)
	{
		echo "<a href='" . $google_link . "'><img class='" . $add_class . "' src='" . get_template_directory_uri() . "/images/gplus.png'></a>";
	}
	else
	{
		echo "<a href='" . $google_link . "'>Google+</a>";
	}
}

function celotehkita_get_facebook($use_icon = false, $add_class='')
{
	$fb_link = get_the_author_meta('facebook');
	if($use_icon)
	{
		echo "<a href='" . $fb_link . "'><img class='" . $add_class . "' src='" . get_template_directory_uri() . "/images/fb.png'></a>";
	}
	else
	{
		echo "<a href='" . $fb_link . "'>Facebook</a>";
	}

}

function celotehkita_get_avatar()
{
	echo get_avatar( get_the_author_meta('id'), 150 );
	// echo '<img src="' . get_template_directory_uri() . '/images/ava.jpeg">';
}
 
function celotehkita_random_number()
{
	$class = 'width'.rand(1,5);
	if(count(explode(' ',get_the_title())) <= 3) 
	{
		$class = 'width4';
	} 
	return $class;
}

function celotehkita_get_bg_author()
{
	return get_the_author_meta('image_url');	
}

function celotehkita_get_reviewed()
{
	$title = '';
	if(is_single( ))
	{
		$title = get_post_meta( get_the_id(), 'reviewed_title', true );
	}
	return $title;
}

function celotehkita_get_reviewed_tagline()
{
	$tagline = '';
	if(is_single())
	{
		$tagline = get_post_meta( get_the_id(), 'reviewed_tagline' , true);		
	}
	return $tagline;
}

function celotehkita_get_gallery()
{
    /* The loop */
    if ( get_post_gallery() ) :
    	?>
        <div class="gallery-content">
			<div class="main-image">
		<?php
        $gallery = get_post_gallery( get_the_ID(), false );
        // $gallery= wp_get_attachment_image( $attachment_id, $size, $icon, $attr );
        $pieces = explode(",", $gallery['ids']);
        /* Loop through all the image and output them one by one */
        foreach( $pieces  as $key => $value)
        {
            echo wp_get_attachment_image( $value, 'full' );
        }
        ?>
            </div>
	        <div class="controllerslider">
	            <div class="next"><img src="<?php echo get_template_directory_uri();?>/images/arrow-right.png"></div>
	            <div class="prev"><img src="<?php echo get_template_directory_uri();?>/images/arrow-left.png"></div>
	        </div>
	    </div>
        <?php
    endif;
}

add_action('wp_ajax_send_contact' , 'send_contact');
add_action('wp_ajax_nopriv_send_contact' , 'send_contact');
function send_contact()
{
	$name = $_post['name']; 
	$subject = $_post['subject']; 
	$address = $_post['address']; 
	$email = $_post['email']; 
	$message = $_post['message']; 

	if(isset($address) || $address != '')
	{
        $response = array(
            "message" => "",
            "status" => "Failed"
        );
    }
	else
	{
	    $to        = 'dev.indramdhani@gmail.com';
	    $from      = 'no-reply@celotehkita.com';
        $subject = "You have a new message from {$name}";

        // Header
        $headers = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=windows-874\n";
        $headers .= "From: ". $email ."  \r\n" .
        $headers .= "\r\n" .
        $headers .= "X-Priority: 1\r\n"; 
        $headers .= "X-MSMail-Priority: High\r\n"; 
        $headers .= "X-Mailer: Just My Server"; 

        $message = "
                    <html>
                        <head>
                            <title>" . $subject . "</title>
                            <style type='text/css'>
                                .header {
                                    font-size: 1.5em;
                                }
                                .content {
                                    font-size: 1em;
                                }
                            </style>
                        </head>
                        <body>
                            <div class='header'>
                                New Message
                            </div>
                            <div class='content'>
                                FIRST NAME: " . $name . "
                                <br>
                                EMAIL: " . $email . "
                                <br>
                                PHONE: " . $phone . "
                                <br>
                                Message: " . $message . "
                                <br>
                            </div>
                        </body>
                    </html>";

        // sendmail
        if(wp_mail($to, $subject, $message, $headers))
        {
            $response = array(
                "message" => "Email Sent Successfully",
                "status" => "Success"
            );
        } 
        else 
        {
            $response = array(
                "message" => "Something went wrong, please try again",
                "status" => "Failed"
            );
        }
	}
	echo json_encode($response);
	exit();
}