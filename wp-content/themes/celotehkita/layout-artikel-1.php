<?php
// default template 1
?>
<div id="singleprimary" class="content-area post-<?php the_ID()?> <?php post_class()?>" layout='<?php echo celotehkita_get_layout();?>'>
    <div id="articles" class="pagecontainer fullheight fullwidth mainpage">
        <div class="page articles">
            <?php
            $style = '';
            if(has_post_thumbnail())
            {
                $large_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
                $style = $large_image[0];
            }?>
            <div class="bg-image" bg-image='<?php echo $style;?>'>
            </div>
            <div class="content-articles">
                &nbsp;
                <div class="header-title">
                	<?php 
                    celotehkita_get_title();
                	?>
                </div>
                <div class= "content">
                    <div class="left-content gotham-light">
                        <?php
                        echo celotehkita_get_gallery();

                    	$txt_content = get_the_content();
                    	echo $txt_content;
                    	?>
                    </div>
                    <div class="contributor gotham-light">
                        <div class="cont-header nevis">
                            About The Author
                        </div>
                        <div class="cont-content">
                            <div class="image">
                                <img src="images/ava.jpeg">
                            </div>
                            <div class="decs">
                                <div class="desc-name gotham-light">
                                    <div class="realname gotham-bold"><?php celotehkita_get_author_name();?></div>
                                    <div class="twitter"><?php celotehkita_get_twitter();?></div>
                                </div>
                                <div class="desc-content barokah">
									<?php celotehkita_get_author_bio();?>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <?php echo get_footer('container');?> 
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>