<?php
// author page
get_header(); ?>
	<div id="singleprimary" class="content-area <?php post_class()?>">
	    <div id="articles" class="pagecontainer fullheight fullwidth mainpage">
	        <div class="page articles">
	            <div class= "bg-image" bg-image='<?php echo celotehkita_get_bg_author();?>'>
	            </div>
	            <div class='author container'>
	            	<h2><?php echo get_the_author();?></h2>
	            	<div class='top'>
	            		<div class='topleft'>
	            			<?php celotehkita_get_avatar();?>
	            		</div>
	            		<div class='topright gotham-light'>
		            		<?php celotehkita_get_author_bio();?>
		        			<ul>
		        				<li><?php celotehkita_get_twitter(true,'greyscale');?></li>
		        				<li><?php celotehkita_get_google(true,'greyscale');?></li>
		        				<li><?php celotehkita_get_facebook(true,'greyscale');?></li>
		        			</ul>
	            		</div>
	            	</div>
	            	<div class='bottom'>
	            		<?php
	            		if(have_posts() )
	            		{
	            			while( have_posts() ) : the_post();
            				?>
            				<div class='item nevis <?php echo celotehkita_random_number();?>'>
            					<?php celotehkita_permalink_title();?>
            					<div class='hover barokah'>
            						<p>go to article >></p>
            					</div>
            				</div>
            				<?php
	            			endwhile;
	            		}
	            		?>
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>
<?php
get_sidebar();
get_footer();
