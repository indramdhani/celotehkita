<?php
/*
    Template Name: About
*/
get_header(); ?>
	<div id="singleprimary" class="content-area <?php post_class()?>">
	    <div id="articles" class="pagecontainer fullheight fullwidth mainpage">
	        <div class="page articles">
	            <div class= "bg-image" bg-image='<?php echo celotehkita_get_bg_author();?>'>
	            </div>
	            <div class='about container'>
	            	<div class='top barokah editor-style'>
	            		<?php 
	            			$title = the_title();
	            			echo $title;
	            		?>	
	            	</div>
	            	<div class='bottom gotham-light'>
	            		<?php 
		            		$content =  the_content();
		            		echo $content;
	            		?>
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>
<?php
get_sidebar();
get_footer();
?>