<?php
/*
    Template Name: Author
*/
get_header(); ?>
	<div id="singleprimary" class="content-area <?php post_class()?>">
	    <div id="articles" class="pagecontainer fullheight fullwidth mainpage">
	        <div class="page articles">
	            <div class= "bg-image" bg-image='<?php echo celotehkita_get_bg_author();?>'>
	            </div>
	            <div class='author container'>
	            	<div class='top barokah editor-style'>
	            		<?php 
	            			$title = the_title();
	            			echo $title;
	            		?>	
	            	</div>
	            	<div class='bottom gotham-light'>
	            		<?php 
		            		$content =  the_content();
		            		echo $content;
	            		?>
	            	</div>
	            	<div class='contributor-page-container'>
					<?php 
						$contributor_ids = get_users( array(
							'fields'  => 'ID',
							'orderby' => 'post_count',
							'order'   => 'DESC',
							'who'     => 'authors',
						) );

						foreach ( $contributor_ids as $contributor_id ) :
							$post_count = count_user_posts( $contributor_id );

							// Move on if user has not published a post (yet).
							if ( ! $post_count ) {
								continue;
							}
						?>

						<div class="contributor-item">
							<div class="contributor-info">
								<div class="contributor-avatar"><?php echo get_avatar( $contributor_id, 132 ); ?></div>
								<div class="contributor-summary">
									<h2 class="contributor-name"><?php echo get_the_author_meta( 'display_name', $contributor_id ); ?></h2>
									<p class="contributor-bio gotham-light">
										<?php echo get_the_author_meta( 'description', $contributor_id ); ?>
									</p>
									<a class="contributor-posts-link gotham-bold" href="<?php echo esc_url( get_author_posts_url( $contributor_id ) ); ?>">
										View Profile
									</a>
								</div><!-- .contributor-summary -->
							</div><!-- .contributor-info -->
						</div><!-- .contributor -->

						<?php
						endforeach;
            		?>	            	
	            	</div>
	        	</div>
	        </div>
	    </div>
	</div>
<?php
get_sidebar();
get_footer();
?>