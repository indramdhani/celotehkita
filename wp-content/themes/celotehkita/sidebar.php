<div class= "menu_nav">
     <div class="menu-left">
        <div class="menu-logo">
            <img src="<?php echo get_template_directory_uri();?>/images/menu.png">
        </div>
        <div class="name-logo bebas">
            <span class="first">celoteh</span>
            <span class="second">kita</span>
        </div>
        <div class="clear"></div>      
     </div>  
     <div class="menu-right">
        <div class="menu-title nevis"><?php echo celotehkita_get_reviewed();?></div>
        <div class="menu-desc gotham-light"><?php echo celotehkita_get_reviewed_tagline();?></div>
     </div> 
     <div class="clear"></div>                  
</div>