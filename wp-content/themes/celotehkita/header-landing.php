<!DOCTYPE html>
<html lang="lang="en-US"">
<head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no,minimum-scale=1.0">
    
    <title>CELOTEHKITA | </title>

    <meta property="og:url" content="" />
    <meta property="og:type" content="" />
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:description" content="" />

    
    <link rel="icon" type="image/ico" href="<?php echo get_template_directory_uri();?>/images/favicon.ico">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/images/favicon.ico">
    
    <link rel="stylesheet"  href="<?php echo get_template_directory_uri();?>/css/landing.css" type="text/css" />
    <?php wp_head();?>
</head>

<body onorientationchange="updateOrientation()">
    <input type="hidden" id="BASE_URL" value="<?php echo get_site_url();?>">
    <input type="hidden" id="TEMPLATE_URL" value="<?php echo get_template_directory_uri();?>">