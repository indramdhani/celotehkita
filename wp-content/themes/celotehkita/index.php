<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">
	<div id="primary" class="content-area <?php post_class();?>">
        <div class= "bg-image" bg-image='<?php echo celotehkita_get_bg_author();?>'></div>
		<div id="content" class="site-content container" role="main">
			<div class="post-container">
		<?php
			if ( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
					// indramdhani : it will return all data that we need
					// $data = celotehkita_get_post_data();
					// indramdhani : to show title
					?>
					<div class='post-item'>
						<div class='post-title'>
							<?php celotehkita_permalink_title();?>
						</div>
						<div class='post-data gotham-light'>
							<div class='post-date'>Posted in <span><?php echo get_the_date();?></span></div>
							<div class='post-author'>By <a href="<?php echo get_author_posts_url(get_the_author_meta('ID' ));?>"><?php echo get_the_author();?></a></div>
						</div>
					</div>
					<?php
				endwhile;
			else :
				// If no content, include the "No posts found" template.
				get_template_part( 'content', 'none' );

			endif;
		?>
			</div>
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
