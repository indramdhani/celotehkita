<?php
/*
    Template Name: Contact
*/
get_header(); ?>
	<div id="singleprimary" class="content-area <?php post_class()?>">
	    <div id="articles" class="pagecontainer fullheight fullwidth mainpage">
	        <div class="page articles">
	            <div class= "bg-image" bg-image='<?php echo celotehkita_get_bg_author();?>'>
	            </div>
	            <div class='about container'>
	            	<div class='top barokah editor-style'>
	            		<?php 
	            			$title = the_title();
	            			echo $title;
	            		?>	
	            	</div>
	            	<div class='bottom gotham-light'>
	            		<div class='form-container'>
	            			<div class='form-item'>
		            			<label>Nama</label>
		            			<input type='text' class='barokah' name='name' placeholder='Nama'>
	            			</div>
	            			<div class='form-item'>
		            			<label>Subjek</label>
		            			<input type='text' class='barokah' name='subject' class='input-item' placeholder='Subjek'>
		            			<input type='text' class='barokah' name='address' class='input-item' placeholder='alamat'>
	            			</div>
	            			<div class='form-item'>
		            			<label>Email</label>
		            			<input type='text' class='barokah' name='email' class='input-item' placeholder='email'>
	            			</div>
	            			<div class='form-item'>
		            			<label>Pesan</label>
		            			<textarea rows='5' cols='50' name='message' class='input-item barokah' placeholder='Pesan kamu...'></textarea>
	            			</div>
	            			<div class='form-item'>
		            			<label></label>
		            			<input type='button' name='send' class='input-item btn gotham-light' value='Kirim'>
		            			<label class='notif'></label>
	            			</div>         			
	            		</div>
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>
<?php
get_sidebar();
get_footer();
?>