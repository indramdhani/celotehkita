<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content='chrome=1'>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0">
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	    <link rel="icon" type="image/ico" href="<?php echo get_template_directory_uri();?>/images/favicon.ico">
	    <link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/images/favicon.ico">

<!-- 	    <meta property="og:url" content="" />
	    <meta property="og:type" content="" />
	    <meta property="og:title" content="" />
	    <meta property="og:image" content="" />
	    <meta property="og:site_name" content="" />
	    <meta property="og:description" content="" /> -->
		<?php wp_head(); ?>
	</head>
	<body  onorientationchange='updateOrientation()' <?php body_class(); ?>>
		<input type='hidden' id='BASE_URL' value='<?php echo get_site_url();?>'>
		<input type='hidden' id='TEMPLATE_URL' value='<?php echo get_template_directory_uri();?>'>
		<div id='skeletonbodycontainer' class='fullwidth mainpage'>
			<div class='skeletonpagecontainer active'>
				<?php echo get_sidebar();?>