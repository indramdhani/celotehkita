<?php
/*
    Template Name: Landing
*/
get_header('landing');
?>
    <div id='landingpage' class='bgimage'>
        <div class= "bg-image">               
            <div class="landing-content">
                <div class="middle-header">
                    WE WILL<span class='a'> BE ON THE LINE</span><span class='b'> SOON</span>
                </div>
                <div class="top-header">
                    "BER-<span class='a'>CELOTEH</span> ITU <span class="b">GRATIS</span>, CARA <span class="c">REVOLUSIONER</span> UNTUK BERBAGI <span class="d">CERITA</span>."
                </div>
                <div class="litle-header">
                    
                </div>
                <div class="bottom-header">
                    -CELOTEH<span>KITA</span>-
                </div>
            </div>

            <div class="socmed">
                <div class="instagram">
                    <a href="http://www.instagram.com">
                        <img src="<?php echo get_template_directory_uri();?>/images/icon-heart-yellow1.png">
                    </a>
                </div>
                <div class="facebook">
                    <a href="http://www.facebook.com/">
                        <img src="<?php echo get_template_directory_uri();?>/images/icon-facebook-yellow.png">
                    </a>
                </div>
                <div class="twitter">
                    <a href="https://www.twitter.com/">
                        <img src="<?php echo get_template_directory_uri();?>/images/icon-tweet-yellow1.png">
                    </a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
<?php
get_footer('landing');
?>
