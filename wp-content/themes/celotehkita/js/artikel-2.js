$(document).ready(function($) {
     initTitleLoad();   
     initslidercontroller();
});
function initTitleLoad()
{
    // febrivalentino: slide page content       
    var animationSpeed = 1300;
    var eas= 'easeInOutCirc';
    var set = setInterval(function(){
        if(document.readyState == 'complete')
        {
            $(document).oneTime(1000, function(){ 
                $(".header-title").animate({right: "0%", opacity: 1},{duration: 2000,easing: eas});
            });
        }
    },1000);
}
function initslidercontroller()
{
    $('.gallery-content .main-image').carouFredSel({
            responsive: true,
            width: '100%',
            // height:'100%',
            prev: '.prev',
            next: '.next',
            swipe: {
                onTouch: true,
                onMouse: true
                
            },
            auto: false,
            items: {
                width: 400,
                height:'auto',
                visible: 1
            }
        });
}