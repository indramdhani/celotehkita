var current_URL = window.location.protocol + "//" + window.location.host + "" + window.location.pathname;
var BASE_URL = $("#BASE_URL").val();
var TEMPLATE_URL = $("#TEMPLATE_URL").val();

var current_layout = '';

$(document).ready(function($) {
	_detect_layout();
	_load_style();
	_load_specific_engine();
	_load_background();
});

function _detect_layout()
{
	if($('#singleprimary').length > 0 && $('#singleprimary').attr('layout') != '')
	{
		current_layout = $('#singleprimary').attr('layout');
	}
}

function _load_style()
{
	// indramdhani : will load specific file css, if needed
	console.log(current_layout);
	if(current_layout != '' && current_layout != 'undefined' && current_layout != undefined)
	{
	    $("head").append('<link rel="stylesheet"  href="'+TEMPLATE_URL+'/css/' + current_layout + '.css" type="text/css" /></head>');
	}
}

function _load_specific_engine()
{
	// indramdhani : specific engine for each layout
	if(current_layout != '' && current_layout != 'undefined' && current_layout != undefined)
	{
		$.getScript(TEMPLATE_URL + '/js/' + current_layout+'.js', function(data, textStatus){
		});
	}
}

function _load_background()
{
	var image_url = $('.bg-image').attr('bg-image');
	if(image_url != '' && image_url != undefined)
	{
		$('.bg-image').css('background-image', 'url(' + image_url + ')');
	}
}