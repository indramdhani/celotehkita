var currentURL = window.location.protocol + "//" + window.location.host + "" + window.location.pathname;
var BASE_URL = $("#BASE_URL").val();
var TEMPLATE_URL = $("#TEMPLATE_URL").val();

// define some variabel here
var animationSpeed = 500;

$(document).ready(function(){
    
    initBrowserSetting();

    initPageSize();

    clickAction();

    hoverAction();

    debugScreenSize();
    
    $(window).resize( $.debounce( animationSpeed, function(){
        initPageSize();
        debugScreenSize();
    }) );
});

//brainware: function to make the browser config specific
function initBrowserSetting()
{
    //firefox
    if(isFirefox())
    {
        debug('using FF');
    }
    //IE
    else if(isIE())
    {
        debug('using IE 9');
        if(isIE('8'))
        {
            debug('using IE 8');
        }
        if(isIE('10'))
        {
            debug('using IE 10');
        }    
    }
    //chrome
    else if(isChrome())
    {
        debug('using chrome');
    }
    //iPad, put here before safari, because iPad is using safari too
    else if(isIpad())
    {
        debug('using iPad');
    }
    //safari
    else if(isSafari())
    {
        debug('using safari');
    }
    else if(isSafariWindow())
    {
        debug('using safari windows');
    }

    // check if mobile
    // Android
    if(isAndroid())
    {
        debug("using android");

        if(isMobile == true || isMobile == 'true' || isMobile == undefined || typeof isMobile == 'undefined')
        {
            // useBackStretch = false;
            // $("head meta[name='viewport']").attr("content","width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0");
        }
        else
        {
            $('meta[name=viewport]').remove();
        }
    }
    // iPhone
    if(isIphone())
    {
        debug("using iPhone");
        if(isMobile == true || isMobile == 'true' || isMobile == undefined || typeof isMobile == 'undefined')
        {
            // $("head meta[name='viewport']").attr("content","width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0");
        }
        else
        {
            $('meta[name=viewport]').remove();
        }
    }
}

//function to set the background images, we are using Backstretch plugin right now to make it faster on chrome
function initBackgroundImage()
{
    if(useBackStretch == true)
    {  
        if(isIpad())
        {
        }
        else
        {
        }
    }
    else
    {
        
        if(isAndroid())
        {
        }
        else if(isIphone())
        {
        }
    }
}

//function to init the page size
function initPageSize()
{
    var windowH = $(window).height(); //set body height to num of page x window h

    var menuH = $(".menu_nav").height();
    console.log (windowH);

    if(!isIphone() || !isAndroid())
    {
        $(".bg-image").css({
            'height': windowH
        });
        $(".content-articles .content").css({
            'top': windowH
        });
    }
}

function _redirect_link(url)
{
    window.location.href=url;
}

//brainware: controller for click event
function clickAction()
{
    // indramdhani : click for function
    $('.author.container .item').on('click', function(){
        var link = $(this).find('a').attr('href');
        _redirect_link(link);
    });
    // indramdhani : form contact  
    $('input[name="send"]').live('click', function(){
        $notif = $('label.notif');
        $notif.fadeOut().removeClass('red').html('Sending your message...').fadeIn();
        var name = $('input[name="name"]').val();
        var subject = $('input[name="subject"]').val();
        var address = $('input[name="address"]').val();
        var email = $('input[name="email"]').val();
        var message = $('textarea[name="message"]').val();

        var valid = true;
        if( name == '' )
        {
            $('input[name="name"]').css('border', '1px solid red');
            valid &= false;
        }
        else
        {
            $('input[name="name"]').css('border', 'none');            
        }
        if( subject == '' )
        {
            $('input[name="subject"]').css('border', '1px solid red');
            valid &= false;
        }
        else
        {
            $('input[name="subject"]').css('border', 'none');
        }
        if( email == '' )
        {
            $('input[name="email"]').css('border', '1px solid red');
            valid &= false;
        }    
        else
        {
            $('input[name="email"]').css('border', 'none');
        }    
        if( message == '' )
        {
            $('textarea[name="message"]').css('border', '1px solid red');
            valid &= false;
        }
        else
        {
            $('textarea[name="message"]').css('border', 'none');
        }

        if(valid)
        {
            $.ajax({
                type: 'POST',
                url: BASE_URL + '/wp-admin/admin-ajax.php',
                data: {action:'send_contact' , name: name, subject: subject, address: address, email: email, message: message},            
                dataType: 'json',
            })
            .done(function(data) {
                if(data.status == 'Success')
                {
                    $notif.removeClass('red');
                }
                else
                {
                    $notif.addClass('red');
                }
                $notif.fadeOut().html(data.message).fadeIn('400', function() {
                    $(document).oneTime(3000, function(){
                        $notif.fadeOut();
                    });
                });
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        }
        else
        {
            $notif.fadeOut().html('Please fill required fields..').addClass('red').fadeIn();
        }
    });
}

function hoverAction()
{
    if(!isIpad())
    {
        // indramdhani : hover in article item author page
        // $('.author.container .item').hover(function() {
        //     $(this).find('.hover').fadeIn(animationSpeed);            
        // }, function() {
        //     $(this).find('.hover').fadeOut(animationSpeed);            
        // });
        $('.author.container .topright img').hover(function() {
            $(this).removeClass('greyscale');
        }, function() {
            $(this).addClass('greyscale');
        });
    }
}

// brainware: resize the font
function initFontResize()
{

}

//brainware: to check the browser
function isIE(version)
{
    var uagent = navigator.userAgent.toLowerCase();
    var uagentversion = navigator.appVersion.toLowerCase();

    if(uagent.search('msie') > -1)
    {
        if(typeof version == 'undefined' || version == undefined)
        {
            return true;
        }
        else
        {
            if(uagentversion.search(version) > -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    else
    {
        return false;
    }
}

function isFirefox()
{
    var uagent = navigator.userAgent.toLowerCase();

    if(uagent.search('firefox') > -1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function isChrome()
{
    var uagent = navigator.userAgent.toLowerCase();

    if(uagent.search('chrome') > -1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function isIpad()
{
    var uagent = navigator.userAgent.toLowerCase();

    if(uagent.search('ipad') > -1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function isIphone()
{
    var uagent = navigator.userAgent.toLowerCase();
    if(uagent.search('iphone') > -1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function isAndroid()
{
    var uagent = navigator.userAgent.toLowerCase();
    if(uagent.search('android') > -1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function isSafari()
{
    var uagent = navigator.userAgent.toLowerCase();

    if(uagent.search('chrome') > -1)
    {
        return false;
    }
    else if(uagent.search('safari') > -1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function isSafariWindow()
{
    var uagent = navigator.userAgent.toLowerCase();

    if (uagent.indexOf("safari/") !== -1 && uagent.indexOf("windows") !== -1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

//display the browser resolution & optimum ratio
function debugScreenSize()
{
    //disable debug
    return false;
    if(true)
    {
        var screenH = $(window).height();
        var screenW = $(window).width();

        var optimalRatio = 4/3;
        var currentRatio = screenW / screenH;

        var difRatio = (optimalRatio - currentRatio).toFixed(3);

        debugMsg = screenW + " x " + screenH + " : " + difRatio;

        if(difRatio < 0.1 && difRatio > -0.1)
        {
            debugMsg += " : optimumRatio";
        }

        //brainware: check the onscreendebug is exist
        var onScreenDebug = jQuery("#onscreendebug2");
        if(onScreenDebug.length == 0)
        {
            jQuery("body").append("<div id='onscreendebug2'></div>");

            jQuery("#onscreendebug2").css({
                position: "fixed",
                top: "60px",
                left: "10px",
                "z-index": "9999",
                "background-color": "#000",
                opacity: 0.5,
                color: "#FFF",
                padding: "10px",
                "font-size": "12px",
                "line-height": "16px"
            });

            jQuery("#onscreendebug2").hover(function(){
                jQuery(this).stop().animate({opacity: 1},500);
            },function(){
                jQuery(this).stop().animate({opacity: 0.5},500);
            });

            jQuery("#onscreendebug2").html(debugMsg + "<br />");
        }

        onScreenDebug.html(debugMsg);
    }
}

//display the current variables
function debugVariables()
{
    return false;
    //currentSubPage
    debugClear();

    debug('currentSubPage');

    for(var i=0; i<currentSubPage.length; i++)
    {
        if(typeof currentSubPage[i] != 'undefined')
        {
            for(var j=0; j<currentSubPage[i].length; j++)
            {
                debug('currentSubPage['+i+']['+j+']: ' + currentSubPage[i][j]);
            }
        }
    }
}

//function to clear the debug (only on screen debug)
function debugClear()
{
    var onScreenDebug = jQuery("#onscreendebug");
    if(onScreenDebug.length != 0)
    {
        jQuery("#onscreendebug").html('');
    }
}

function debug(debugMsg,useOnScreenDebug,useAlert)
{
    //disable debug
    return false;
    if(useOnScreenDebug == undefined)
    {
        useOnScreenDebug = !false;
    }

    if(useAlert == undefined)
    {
        useAlert = false;
    }

    if(useAlert)
    {
        alert(debugMsg);
    }
    else if(useOnScreenDebug)
    {
        //brainware: check the onscreendebug is exist
        var onScreenDebug = jQuery("#onscreendebug");
        if(onScreenDebug.length == 0)
        {
            jQuery("body").append("<div id='onscreendebug'></div>");

            jQuery("#onscreendebug").css({
                position: "fixed",
                top: "10px",
                right: "10px",
                "z-index": "9999",
                "background-color": "#000",
                opacity: 0.5,
                color: "#FFF",
                padding: "10px",
                "font-size": "12px",
                "line-height": "16px"
            });

            jQuery("#onscreendebug").hover(function(){
                jQuery(this).stop().animate({opacity: 1},500);
            },function(){
                jQuery(this).stop().animate({opacity: 0.5},500);
            });

            jQuery("#onscreendebug").html(debugMsg + "<br />");

            //click to reset debug msg
            jQuery("#onscreendebug").click(function(){
                jQuery(this).html('');
            });
        }

        onScreenDebug.html(onScreenDebug.html() + debugMsg + "<br />");
    }
    else
    {
        // console.log(debugMsg);
        return false;
    }
}

var useLandscape = false;

function updateOrientation()
{
    //execute only on iPad and iPhone and android
    if(!isIpad() && !isIphone() && !isAndroid())
    {
        return false;
    }

    var degree = window.orientation;

    if(degree == "90" || degree == "-90")
    {
        useLandscape = true;
    }
    else
    {
        useLandscape = false;
    }

    // activate error message on iPad


    if(isMobile == true || isMobile == 'true' || isMobile == undefined || typeof isMobile == 'undefined')
    {
        // activate error message on iPhone and android
        if((useLandscape && isIphone()) || (useLandscape && isAndroid()))
        {
            showErrorMsgOnMobile('portrait');
        }
        else
        {
            removeErrorMsgOnMobile();
        }
    }
    else
    {
        if((!useLandscape && isIphone()) || (!useLandscape && isAndroid()))
        {
            showErrorMsgOnMobile('landscape');
        }
        else
        {
            removeErrorMsgOnMobile();
        }
    }

    if (isIpad())
    {
        if(!useLandscape)
        {
            
            showErrorMsgOnIpad();
        }
        else
        {
            removeErrorMsgOnIpad();
        }
    }   
}


function showErrorMsgOnIpad()
{
    // return false;
    var errorMsg = 'Please turn your iPad into landscape mode for a better view <br><br><img src="'+ TEMPLATE_URL +'/images/logo-yellow.png"><br><br>United Backpackers';
    // alert("Please turn your iPad into landscape mode for better view");
    // debug (errorMsg);
    var overlay = $("<div id='msgLoverlay'></div>").appendTo("body");
    var msg = $("<div id='msgInfo'>" + errorMsg + "</div>").appendTo(overlay);

    $(document).oneTime(500,function(){
        $(window).scrollTop(0);

        var $fixedElement = $('#msgLoverlay');
        var topScrollTarget = 0;

        $fixedElement.css({ "position": "relative" });
        window.scroll(0, topScrollTarget );
        $fixedElement.css({ "position": "fixed" });

        $("#wrapper").css({
            top: "0px",
            left: "0px"
        });
    });
}

function showErrorMsgOnMobile(rotation)
{
    // return false;
    if (rotation=="portrait")
    {

        var errorMsg = 'Please turn your device into '+ rotation +' mode for a better view  <br><br><img src="'+ TEMPLATE_URL +'/images/logo-yellow.png"><br><br>United Backpackers';
    }
    else 
    {
                var errorMsg = 'Please turn your device into '+ rotation +' mode for a better view <br><br><img src="'+ TEMPLATE_URL +'/images/logo-yellow.png"><br><br>United Backpackers<br><br><a href=#><img class="backtomobile" src="'+ TEMPLATE_URL +'/images/backtomobile.png"></a>';
    }
    
    // alert("Please turn your iPad into landscape mode for better view");
    // debug (errorMsg);
    var overlay = $("<div id='msgLoverlay'></div>").appendTo("body");
    var msg = $("<div id='msgInfo'>" + errorMsg + "</div>").appendTo(overlay);

    $(document).oneTime(500,function(){
        $(window).scrollTop(0);

        var $fixedElement = $('#msgLoverlay');
        var topScrollTarget = 0;

        $fixedElement.css({ "position": "relative" });
        window.scroll(0, topScrollTarget );
        $fixedElement.css({ "position": "fixed" });

        $("#wrapper").css({
            top: "0px",
            left: "0px"
        });
    });

    if(isIphone() || isAndroid()) 
    {
        $('#msgInfo').css('top','7%')
    }
}

function removeErrorMsgOnIpad()
{
    $("#msgLoverlay").fadeOut(800,function(){
        $(this).remove();
    });
}

function removeErrorMsgOnMobile()
{
    $("#msgLoverlay").fadeOut(800,function(){
        $(this).remove();
    });
}
