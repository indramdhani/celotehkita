<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 * tampered so it can used multi layout
 */

get_header();
	// Start the Loop.
	while ( have_posts() ) : the_post();
		// indramdhani : use this to get the layout 
		if(celotehkita_get_layout())
		{
			get_template_part( 'layout', celotehkita_get_layout());
		}
		else
		{
			get_template_part( 'layout', 'default');
		}

		// If comments are open or we have at least one comment, load up the comment template.
		// if ( comments_open() || get_comments_number() ) {
			// comments_template();
		// }
	endwhile;
get_sidebar();
get_footer();
?>
